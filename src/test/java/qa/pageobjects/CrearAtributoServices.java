package qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CrearAtributoServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private CrearAtributoPage crearAtributoPage;

    public void writeInputCrearCodigo(String codigo){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearAtributoPage.getInputCrearCodigo()));
        this.crearAtributoPage.getInputCrearCodigo().sendKeys(codigo);
    }

    public void writeInputCrearDescripcion(String descripcion){
        this.crearAtributoPage.getInputCrearDescripcion().sendKeys(descripcion);
    }

    public void selectTipoAtributo(String atributo){
        new Select(this.crearAtributoPage.getSelectTipoAtributo()).selectByVisibleText(atributo);
    }

    public void selectRamo(String ramo){
        new Select(this.crearAtributoPage.getSelectRamo()).selectByVisibleText(ramo);
    }

    public void selectEstado(String estado){
        new Select(this.crearAtributoPage.getSelectEstado()).selectByVisibleText(estado);
    }

    public void selectEscala(String escala){
        new Select(this.crearAtributoPage.getSelectEscala()).selectByVisibleText(escala);
    }

    public void clickOnButtonCancelar() {
        this.crearAtributoPage.getButtonCancelar().click();
    }

    public void clickOnButtonConfirmar() {
        this.crearAtributoPage.getButtonConfirmar().click();
    }

    public String getMensajeCrear(){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearAtributoPage.getMensajeCrear()));
        return this.crearAtributoPage.getMensajeCrear().getText();
    }

    public void clickOnCerrarMensaje() {
        this.crearAtributoPage.getMensajeCerrar().click();
    }


}
