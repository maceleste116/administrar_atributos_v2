package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class CrearAtributoPage extends PageBase {

    @Autowired
    public CrearAtributoPage(WebDriver driver){
        super(driver);
    }

    @FindBy(id="crear-atributo-input-codigo")
    private WebElement inputCrearCodigo;

    @FindBy(id="crear-atributo-input-descripcion")
    private WebElement inputCrearDescripcion;

    @FindBy(id="crear-atributo-select-idTipoAtributo")
    private WebElement selectTipoAtributo;

    @FindBy(id="crear-atributo-select-idRamo")
    private WebElement selectRamo;

    @FindBy(id="crear-atributo-select-estado")
    private WebElement selectEstado;

    @FindBy(id="crear-atributo-select-idEscala")
    private WebElement selectEscala;

    @FindBy(id="crear-atributo-boton-Cancelar")
    private WebElement buttonCancelar;

    @FindBy(id="crear-atributo-boton-Confirmar")
    private WebElement buttonConfirmar;

    @FindBy(id="crear-atributo-modal-mensaje")
    private WebElement mensajeCrear;

    @FindBy(id="crear-atributo-modal-modal-button-close")
    private WebElement mensajeCerrar;



}
