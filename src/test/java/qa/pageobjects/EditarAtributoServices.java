package qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EditarAtributoServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private EditarAtributoPage editarAtributoPage;

    public void writeInputEditarCodigo(String codigo){
        this.wait.until(ExpectedConditions.visibilityOf(this.editarAtributoPage.getInputEditarCodigo()));
        this.editarAtributoPage.getInputEditarCodigo().sendKeys(codigo);
    }

    public void writeInputEditarDescripcion(String descripcion){
        //this.wait.until(ExpectedConditions.visibilityOf(this.editarAtributoPage.getInputEditarDescripcion()));
        this.editarAtributoPage.getInputEditarDescripcion().sendKeys(descripcion);
    }

    public void selectEditarTipoAtributo(String atributo){
        new Select(this.editarAtributoPage.getSelectEditarTipoAtributo()).selectByVisibleText(atributo);
    }

    public void selectEditarRamo(String ramo){
        new Select(this.editarAtributoPage.getSelectEditarRamo()).selectByVisibleText(ramo);
    }

    public void selectEditarEstado(String estado){
        new Select(this.editarAtributoPage.getSelectEditarEstado()).selectByVisibleText(estado);
    }

    public void selectEditarEscala(String escala){
        new Select(this.editarAtributoPage.getSelectEditarEscala()).selectByVisibleText(escala);
    }

    public void clickOnButtonCancelar() {
        this.editarAtributoPage.getButtonCancelar().click();
    }

    public void clickOnButtonConfirmar() {
        this.editarAtributoPage.getButtonConfirmar().click();
    }

    public String getMensajeEditar(){
        this.wait.until(ExpectedConditions.visibilityOf(this.editarAtributoPage.getMensajeEditar()));
        return this.editarAtributoPage.getMensajeEditar().getText();
    }

    public void clickOnCerrarMensaje() {
        this.editarAtributoPage.getMensajeCerrar().click();
    }



}
