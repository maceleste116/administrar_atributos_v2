package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.CrearAtributoServices;
import qa.pageobjects.HomeAtributoServices;

@Component
public class CrearAtributo {

    @Autowired
    private HomeAtributoServices homeAtributoServices;

    @Autowired
    private CrearAtributoServices crearAtributoServices;

    public void withInfoRequired(){
        homeAtributoServices.clickOnButtonCrear();
        crearAtributoServices.writeInputCrearCodigo("AAAAAT01");
        crearAtributoServices.writeInputCrearDescripcion("AAAAAT01");
        crearAtributoServices.selectTipoAtributo("Porcentaje");
        crearAtributoServices.selectRamo("Automotores");
        crearAtributoServices.selectEstado("Activo");
        crearAtributoServices.selectEscala("Valor");
        crearAtributoServices.clickOnButtonConfirmar();
    }

    public void withOutInfoRequired(){
        homeAtributoServices.clickOnButtonCrear();
        crearAtributoServices.writeInputCrearCodigo("001");
        crearAtributoServices.writeInputCrearDescripcion("");
        crearAtributoServices.selectTipoAtributo("Porcentaje");
        crearAtributoServices.selectRamo("Automotores");
        crearAtributoServices.selectEstado("Activo");
        crearAtributoServices.selectEscala("Valor");
        crearAtributoServices.clickOnButtonConfirmar();
    }

    public void withCodigoExistente(){
        homeAtributoServices.clickOnButtonCrear();
        crearAtributoServices.writeInputCrearCodigo("AAAAAT01");
        crearAtributoServices.writeInputCrearDescripcion("AAAAAT01");
        crearAtributoServices.selectTipoAtributo("Porcentaje");
        crearAtributoServices.selectRamo("Automotores");
        crearAtributoServices.selectEstado("Activo");
        crearAtributoServices.selectEscala("Valor");
        crearAtributoServices.clickOnButtonConfirmar();
    }

}
